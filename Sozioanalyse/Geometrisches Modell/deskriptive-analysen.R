# *********************************************************************************************
# DESKRIPTVE ANALYSEN — Studienalltag Wintersemester 15/16 ------------------------------------
# *********************************************************************************************

# Paket laden ---------------------------------------------------------------------------------

library("TimeSpaceAnalysis")

# Basisdaten ----------------------------------------------------------------------------------

datensatz_allgemeine_angaben <-
  read_rds("repository/datensatz_allgemeine_angaben.rds")
datensatz_demografische_angaben <-
  read_rds("repository/datensatz_demografische_angaben.rds")
datensatz_eltern <-
  read_rds("repository/datensatz_eltern.rds")
datensatz_kindheit_und_jugend <-
  read_rds("repository/datensatz_kindheit_und_jugend.rds")
datensatz_schulzeit <-
  read_rds("repository/datensatz_schulzeit.rds")

# Datensatz zusammenstellen -------------------------------------------------------------------

daten_deskriptive_analysen <-
  bind_cols(
    datensatz_allgemeine_angaben,
    datensatz_demografische_angaben,
    datensatz_eltern,
    datensatz_kindheit_und_jugend,
    datensatz_schulzeit
  ) %>%
  filter(semester_id_1516 == "Wintersemester-15-16")

# Demografische Angaben -----------------------------------------------------------------------
#
# Idee: Datensatz zusammengestellt als RDS übergeben und dann den Code direkt laden im RMD.

# Geschlecht ----------------------------------------------------------------------------------

plot_barplot(daten_deskriptive_analysen, geschlecht) +
  ylab("Studierende")


# Alter ---------------------------------------------------------------------------------------

plot_barplot(daten_deskriptive_analysen, "alter")


# Schulabschlüsse -----------------------------------------------------------------------------

df_schulabschluss <-
  daten_deskriptive_analysen %>%
  mutate(
    schulabschluss_vater = fct_relevel(
      schulabschluss_vater,
      "Abitur",
      "Realschule",
      "Hauptschule",
      "keinen Schulabschluss",
      "unbekannt"),
    schulabschluss_vater = fct_recode(
      schulabschluss_vater,
      "keinen" = "keinen Schulabschluss"
    ),
    schulabschluss_mutter = fct_relevel(
      schulabschluss_mutter,
      "Abitur",
      "Realschule",
      "Hauptschule",
      "keinen Schulabschluss",
      "unbekannt"),
    schulabschluss_mutter = fct_recode(
      schulabschluss_mutter,
      "keinen" = "keinen Schulabschluss"
    )
  )



# Facet Visualisierung machen!
# Am besten dann mit Linien und Rahmung.
# Dazu daten aufbereiten und dann geom_col nutzen!
#
# Idee: plot_bar_facet, bei dem man den Datensatz, die Spalten und die

df_mutter <- tibble(
  schulabschluss = df_schulabschluss$schulabschluss_mutter,
  elternteil = "Mutter"
)
df_vater <- tibble(
  schulabschluss = df_schulabschluss$schulabschluss_vater,
  elternteil = "Vater"
)

df_facet_schulabschluss <-
  bind_rows(
    df_mutter,
    df_vater
  )

df_facet_schulabschluss

df_facet_schulabschluss %>% count(schulabschluss, elternteil)


df_facet_schulabschluss

ggplot(
  df_facet_schulabschluss,
  aes(schulabschluss)
) +
  geom_bar() +
  geom_text(
     aes(label=..count..),
     stat='count',
     vjust = 0
 ) +
  facet_wrap(~elternteil) +
  cowplot::theme_minimal_hgrid()

plot_barplot(df_facet_schulabschluss, schulabschluss) #+
  facet_wrap(~elternteil)


# Lösung mit patchwork, aber Vergleich schlechter

p_vater <- plot_barplot(df_schulabschluss, "schulabschluss_vater") +
  ggtitle("Vater")
p_vater

p_mutter <- plot_barplot(df_schulabschluss, "schulabschluss_mutter") +
  ggtitle("Mutter")
p_mutter

p_vater + p_mutter

# Flip test -----------------------------------------------------------------------------------
#
# @TODO: Implementieren in die Funktion


df_cat <-
  daten_deskriptive_analysen %>%
  select(var = geschlecht) %>%
  mutate_all( fct_explicit_na, na_level = "fehlend" ) %>%
  count(var, name = "abs") %>%
  mutate(
    rel = abs / sum(abs),
    var = fct_relevel(var, "fehlend", "männlich", "weiblich")
  )

if (!show_missing) {
  df_cat <-
    df_cat %>%
    filter(var != "fehlend")
}

df_cat_sum <-
  df_cat %>%
  summarise_at(2:3, sum) %>%
  add_column(var = "geschlecht", .before = 1)

p <-
  ggplot(df_cat, aes(var, abs)) +
  geom_bar(
    stat = "identity",
    width = .7
  ) +
  xlab("") +
  ylab("") +
  geom_text(
    aes(
      # y = abs + 5 * 4,
      # FLIP,
      y = abs + 2,
      label = abs
    ),
    family = "Fira Sans",
    size = 4,
    nudge_x = 0.2,
    # FLIP
    hjust = 0
  ) +
  geom_text(
    aes(
      # y = abs + 5 * 1.5,
      # FLIP
      y = abs + 2,
      label = paste0(round(rel * 100, 1), " %")
    ),
    #position = "stack",
    family = "Fira Sans",
    size = 3,
    nudge_x = -0.2,
    # FLIP
    hjust = 0
  ) +
  # FLIPPING
  coord_flip()
p

